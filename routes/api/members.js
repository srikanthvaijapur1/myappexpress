const express = require("express");
const router = express.Router();
const members = require("../../member");
const uuid = require("uuid")

//Get all members
router.get('/', (req, res) => {
    res.json(members);
})

//Get Single Memeber
router.get('/:id', (req, res) => {
    const found = members.some((member) => member.id === parseInt(req.params.id))
    if (found) {
        res.json(members.filter((member) => member.id === parseInt(req.params.id)))
    } else {
        res.status(400).json({
            message: "Id not found "
        })
    }

})

//create Member
router.post('/', (req, res) => {
    const newMember = {
        id: uuid.v4(),
        name: req.body.name,
        email: req.body.email,
        status: 'active'
    }
    if (!newMember.name || !newMember.email) {
        return res.status(400).json({
            msg: 'Please include a name and email'
        })
    } else {
        members.push(newMember);
    }
    res.json(members);
});



//Update Member
router.put('/:id', (req, res) => {
    const found = members.some((member) => member.id === parseInt(req.params.id))
    if (found) {
        const updateMember = req.body;
        members.forEach((member) => {
            if (member.id === parseInt(req.params.id)) {
                member.name = updateMember.name ? updateMember.name : member.name;
                member.email = updateMember.email ? updateMember.email : member.email;
                res.json({
                    msg: "updated successfully",
                    member
                })
            }
        })
    } else {
        res.status(400).json({
            msg: `no member with id:${req.params.id}`
        })
    }
})


//Delete Memeber
router.delete("/:id", (req, res) => {
    const found = members.some((member) => member.id === parseInt(req.params.id));
    if (found) {
        res.json({
            msg: "succefully deleted",
            members: members.filter((member) => member.id !== parseInt(req.params.id))
        });
    } else {
        res.status(400).json({
            msg: `id not found`
        })
    }
})


module.exports = router;