const express = require('express');
const path = require("path");
const members = require("./member");
const logger = require("./middleware/logger");

const app = express();
const port = process.env.PORT || 3000;



//Init middleware
app.use(logger);

//Body Parser Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }))


//Set Static folder 
app.use(express.static(path.join(__dirname, 'public')));

//Members API Routes
app.use("/api/members", require("./routes/api/members"));

app.listen(port, () => {
  console.log(`app listening at http://localhost:${port}`)
})